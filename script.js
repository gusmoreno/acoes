function pesquisar(){
  
    var tickerElement = document.getElementById('ticker') //Salva nas variáveis os tickers digitados no html
    var ticker = tickerElement.value
    var tickerElement2 = document.getElementById('ticker2')
    var ticker2 = tickerElement2.value
    

    var periodoElement = document.getElementById('periodo')//Salva na variável o período desejado
    var periodo = periodoElement.value

    // ERRO caso o ticker for vazio;
    if(ticker == 0 && ticker2 == 0){
        alert('Digite um ticker!')
    }
    if(periodo == 0){
        alert("Selecione um período!")
    }
    let apiKey = "lXbIEo4zTv9oV7BN7G6WVoPdoFoXLNA7vIoLZGze"; //Salva os dados chave da api do Yahoo finance.
    let baseURL = 'https://yfapi.net';
    var options = {
        method: 'GET',
        headers: {
            "x-api-key": apiKey
        }
    }

    cotacao(ticker, ticker2, baseURL, options) // Chama o método e seus parâmetros
    grafico(ticker, ticker2, baseURL, options, periodo)
    

}
  
function diasUteis(quantidade, hora){ //Considera apenas de segunda a sexta-feira e o dia atual só é contemplado após as 18h
        
    var dia = moment()
    if (hora <18) {
    dia.subtract(1, 'days')
    }
    var datas = []
    
    while(quantidade > 0){
        if(dia.isoWeekday()!==6 && dia.isoWeekday() !==7){
        datas.unshift(dia.format('DD/MM/YY'))
        quantidade -= 1
      
    }
    dia.subtract(1, 'days')
    
    }
    return datas
    
}


function cotacao(ticker, ticker2, baseURL, options) {
    var preco = document.getElementById('preco')
    
    var url = `${baseURL}/v6/finance/quote?region=US&lang=en&symbols=${ticker}%2C${ticker2}`;

    fetch(url, options)
    .then(res => res.json())
    .then(data => {
        
        var result = data.quoteResponse.result
        console.log(result)
       
        var resultado = ''

        for(var i = 0; i < result.length; i++){

            var string = 'Empresa: ' + result[i].longName + ' - Preço USD: ' + result[i].ask +'<br>'; 
            resultado += string
          
            }
            
            preco.innerHTML = resultado
          
    })

    .catch(error => {
        console.log('error')
        console.log(error)
    })
}




function grafico(ticker, ticker2, baseURL, options, periodo) {
    var canvas = document.getElementById('dadosGrafico').getContext('2d');
    var canvas2 = document.getElementById('dadosGrafico1').getContext('2d');
    var url = `${baseURL}/v8/finance/chart/${ticker}?comparisons=${ticker2}&range=${periodo}&region=US&interval=1d&lang=en&events=div%2Csplit`

    fetch(url, options)
    .then(res => res.json())
    .then(data => {
        var result = data.chart.result[0]
        console.log(result)
       
       
        var dadosGrafico = result.comparisons[0].close
        var dadosGrafico1 = result.indicators.quote[0].close
    
        var quantidade = dadosGrafico.length
        var hora = new Date().getHours()
       
       
            
        var week = diasUteis(quantidade, hora)   
        
        console.log(dadosGrafico)
        console.log(dadosGrafico1)
        console.log(week)   
              
                                    
                              
        var canvas = document.getElementById('dadosGrafico').getContext('2d');
            let chart = new Chart(canvas, {
            type: 'line',

            
                                
            data: {
                labels: week, 
                            
                datasets: [
                    {
                        label: ticker2,
                        borderColor: '#DE1A1A',
                        data: dadosGrafico,
                    
                    },
                    {
                        label: ticker,
                        borderColor: '#9999ff',
                        data: dadosGrafico1,
                    }
                ]
            },
            
           

        });
       
    })
    .catch(error => {
        console.log('error')
        console.log(error)

           
    })
    
   
}
  


 